import { StatusBar } from "expo-status-bar";
import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  Button,
  TouchableOpacity,
} from "react-native";
 
export default function App() {
  const [usrername, setUsername] = useState("");
  const [password, setPassword] = useState("");
   const [email, setEmail] = useState("");
 
  return (
    <View style={styles.container}>
<Text style ={{fontSize: 30,color: "Black", marginBottom: 50,marginTop:50}}
      > JOIN US !!!
      </Text>
 
      <StatusBar style="auto" />
      <View style={styles.inputView}>
        <TextInput
          style={styles.TextInput}
          placeholder="e-mail"
          placeholderTextColor="#11"
          secureTextEntry={true}
          onChangeText={(password) => setPassword(password)}
        />
      </View>
      <View style={styles.inputView}> 
        <TextInput
          style={styles.TextInput}
          placeholder="name"
          placeholderTextColor="#11"
          onChangeText={(username) => setUsername(username)}
        />
      </View>
 
      <View style={styles.inputView}>
        <TextInput
          style={styles.TextInput}
          placeholder="password"
          placeholderTextColor="#11"
          secureTextEntry={true}
         
        />
      </View>

      <View style={styles.inputView}>
        <TextInput
          style={styles.TextInput}
          placeholder="re-type password"
          placeholderTextColor="#11"
          secureTextEntry={true}
          
        />
      </View>
 
      
 
      <TouchableOpacity style={styles.loginBtn}>
        <Text style={styles.loginText}>Sign-Up</Text>
      </TouchableOpacity>

      <TouchableOpacity>
        <Text style={styles.reg_button}>Sign-In</Text>
      </TouchableOpacity>
    </View>
  );
}
 
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#",
    alignItems: "center",
    justifyContent: "center",
  },
 
  image: {
    marginBottom: 30,
  },
 
  inputView: {
    backgroundColor: "#f6f2e9",
    borderRadius: 30,
    width: "80%",
    height: 45,
    marginBottom: 20,
    alignItems: "align",
  },
 
  TextInput: {
    height: 50,
    flex: 1,
    padding: 10,
    marginLeft: 5,
  },
 
  forgot_button: {
    height: 30,
    marginBottom: 30,
  },
 
  loginBtn: {
    width: "50%",
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 30,
    backgroundColor: "#FFCDCB",
  },

  reg_button: {
    height: 30,
    marginTop: 10,
    marginBottom: 30,
  },
});